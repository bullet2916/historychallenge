package events;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import util.*;

public class Events extends HttpServlet {

   public class Event implements Comparable<Event> {
      String date;
      String theEvent;
      Map<String,Integer> months;

      public Event(String date,
                   String theEvent) {
         this.date = date;
         this.theEvent = theEvent;

         months = new HashMap<>();

         months.put("january",1);
         months.put("february",2);
         months.put("march",3);
         months.put("april",4);
         months.put("may",5);
         months.put("june",6);
         months.put("july",7);
         months.put("august",8);
         months.put("september",9);
         months.put("october",10);
         months.put("november",11);
         months.put("december",12);
      }

      public String toString() {
         return(date + " " + theEvent + "<br>");
      }

      public int compareTo(Event event) {
         String[] tokens1 = date.trim().split(" ");

         String[] tokens2 = event.date.trim().split(" ");

         try {
            if (tokens1.length == 3 && tokens2.length == 3) {
               int year1 = Integer.parseInt(tokens1[2]);

               int month1 = months.get(tokens1[0].toLowerCase());

               int day1 = Integer.parseInt(tokens1[1].substring(0,tokens1[1].length()-1));

               int year2 = Integer.parseInt(tokens2[2]);

               int month2 = months.get(tokens2[0].toLowerCase());

               int day2 = Integer.parseInt(tokens2[1].substring(0,tokens2[1].length()-1));

               if (year1 < year2)
                  return(-1);
               else if (year1 > year2)
                  return(1);
               else if (month1 < month2)
                  return(-1);
               else if (month1 > month2)
                  return(1);
               else if (day1 < day2)
                  return(-1);
               else if (day1 > day2)
                  return(1);
               else
                  return(0);
            } else if (tokens1.length == 1 && tokens2.length == 1) {
               int year1 = Integer.parseInt(date.trim());

               int year2 = Integer.parseInt(event.date.trim());

               if (year1 < year2)
                  return(-1);
               else if (year1 > year2)
                  return(1);
               else
                  return(0);
            } else if (tokens1.length == 3 && tokens2.length == 1) {
               int year1 = Integer.parseInt(tokens1[2]);

               int year2 = Integer.parseInt(tokens2[0]);

               if (year1 < year2)
                  return(-1);
               else if (year1 > year2)
                  return(1);
               else 
                  return(0);
            } else if (tokens1.length == 1 && tokens2.length == 3) {
               int year1 = Integer.parseInt(tokens1[0]);
 
               int year2 = Integer.parseInt(tokens2[2]);

               if (year1 < year2)
                  return(-1);
               else if (year1 > year2)
                  return(1);
               else
                  return(0);
            } else
               return(0);
         } catch (Exception e) {
            return(0);
         }
      }
   }

   public Set<Event> getEvents(Connection connection) {
      List<Event> events = new ArrayList<>();

      String temp = "SELECT * FROM events";

      try {
         PreparedStatement statement = connection.prepareStatement(temp);

         ResultSet rs = statement.executeQuery();

         while (rs.next())
            events.add(new Event(rs.getString("date"),rs.getString("event")));
      } catch (SQLException se) {
         System.out.println(se);
      }

      int[] indices = new int[10];

      indices[0] = (int)(events.size()*Math.random());

      for (int counter=1;counter<indices.length;counter++) {
         indices[counter] = indices[counter-1];

         boolean same = true;

         boolean shortEnough = false;

         while (same || !shortEnough) {
            indices[counter] = (int)(events.size()*Math.random());

            System.out.println(events.get(indices[counter]).theEvent.length());

            if (events.get(indices[counter]).theEvent.length() > 100)
               shortEnough = false;
            else
               shortEnough = true;

            System.out.println(shortEnough);

            same = false;

            for (int counter1=0;counter1<counter;counter1++)
               if (indices[counter] == indices[counter1])
                  same = true;
         }            
      }

      Set<Event> output = new TreeSet<>();

      for (int counter=0;counter<indices.length;counter++)
         output.add(events.get(indices[counter]));

      return(output);
   }

   public void doGet(HttpServletRequest request,
                     HttpServletResponse response) throws IOException,ServletException {
      SQL.loadDriver();

      Connection connection = SQL.getConnection("127.0.0.1","cfits","cfits","cfits");

      Set<Event> events = getEvents(connection);

      response.setContentType("text/html");

      PrintWriter output = response.getWriter();

      for (Event event : events)
         output.println(event);
   }

}