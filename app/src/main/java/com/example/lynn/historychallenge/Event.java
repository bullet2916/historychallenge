package com.example.lynn.historychallenge;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by lynn on 3/13/2015.
 */
public class Event implements Comparable<Event> {
    String date;
    String theEvent;
    Map<String,Integer> months;

    public Event(String date,
                 String theEvent) {
        this.date = date;
        this.theEvent = theEvent;

        months = new HashMap<>();

        months.put("january",1);
        months.put("february",2);
        months.put("march",3);
        months.put("april",4);
        months.put("may",5);
        months.put("june",6);
        months.put("july",7);
        months.put("august",8);
        months.put("september",9);
        months.put("october",10);
        months.put("november",11);
        months.put("december",12);
    }

    public String toString() {
        return(date + " " + theEvent);
    }

    public int compareTo(Event event) {
        String[] tokens1 = date.trim().split(" ");

        String[] tokens2 = event.date.trim().split(" ");

        try {
            if (tokens1.length == 3 && tokens2.length == 3) {
                int year1 = Integer.parseInt(tokens1[2]);

                int month1 = months.get(tokens1[0].toLowerCase());

                int day1 = Integer.parseInt(tokens1[1].substring(0,tokens1[1].length()-1));

                int year2 = Integer.parseInt(tokens2[2]);

                int month2 = months.get(tokens2[0].toLowerCase());

                int day2 = Integer.parseInt(tokens2[1].substring(0,tokens2[1].length()-1));

                if (year1 < year2)
                    return(-1);
                else if (year1 > year2)
                    return(1);
                else if (month1 < month2)
                    return(-1);
                else if (month1 > month2)
                    return(1);
                else if (day1 < day2)
                    return(-1);
                else if (day1 > day2)
                    return(1);
                else
                    return(0);
            } else if (tokens1.length == 1 && tokens2.length == 1) {
                int year1 = Integer.parseInt(date.trim());

                int year2 = Integer.parseInt(event.date.trim());

                if (year1 < year2)
                    return(-1);
                else if (year1 > year2)
                    return(1);
                else
                    return(0);
            } else if (tokens1.length == 3 && tokens2.length == 1) {
                int year1 = Integer.parseInt(tokens1[2]);

                int year2 = Integer.parseInt(tokens2[0]);

                if (year1 < year2)
                    return(-1);
                else if (year1 > year2)
                    return(1);
                else
                    return(0);
            } else if (tokens1.length == 1 && tokens2.length == 3) {
                int year1 = Integer.parseInt(tokens1[0]);

                int year2 = Integer.parseInt(tokens2[2]);

                if (year1 < year2)
                    return(-1);
                else if (year1 > year2)
                    return(1);
                else
                    return(0);
            } else
                return(0);
        } catch (Exception e) {
            return(0);
        }
    }

    public static Set<Event> getEvents(SQLiteDatabase database) {
        List<Event> events = new ArrayList<>();

        String[] columns = {"data","event"};

        Cursor cursor = database.query("events",columns,"",null,"","","");

        if (cursor.moveToNext())
            do {
                String date = cursor.getString(cursor.getColumnIndex("date"));
                String event = cursor.getString(cursor.getColumnIndex("event"));

                events.add(new Event(date,event));
            } while (cursor.moveToNext());

        int[] indices = new int[10];

        indices[0] = (int)(events.size()*Math.random());

        for (int counter=1;counter<indices.length;counter++) {
            indices[counter] = indices[counter-1];

            boolean same = true;

            boolean shortEnough = false;

            while (same || !shortEnough) {
                indices[counter] = (int)(events.size()*Math.random());

                System.out.println(events.get(indices[counter]).theEvent.length());

                if (events.get(indices[counter]).theEvent.length() > 100)
                    shortEnough = false;
                else
                    shortEnough = true;

                System.out.println(shortEnough);

                same = false;

                for (int counter1=0;counter1<counter;counter1++)
                    if (indices[counter] == indices[counter1])
                        same = true;
            }
        }

        Set<Event> output = new TreeSet<>();

        for (int counter=0;counter<indices.length;counter++)
            output.add(events.get(indices[counter]));

        return(output);
    }



}
