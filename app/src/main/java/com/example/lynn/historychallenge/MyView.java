package com.example.lynn.historychallenge;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by lynn on 3/13/2015.
 */
public class MyView extends LinearLayout {

   public MyView(Context context) {
       super(context);

       addView(MainActivity.textView);

       MyDatabaseHelper helper = new MyDatabaseHelper(context,"events");

       SQLiteDatabase database = helper.getWritableDatabase();

       MainActivity.textView.setText(database.toString());

       Cursor cursor = database.query("events",new String[]{"date","events"},"",null,"","","");

       if (cursor.moveToFirst())
           do {
               String date = cursor.getString(cursor.getColumnIndexOrThrow("data"));
               String event = cursor.getString(cursor.getColumnIndexOrThrow("event"));

               MainActivity.textView.setText(date + " " + event);
           } while (cursor.moveToNext());

    //   Set<Event> events = Event.getEvents(database);

    //   List<Event> list = new ArrayList<>();

    //   for (Event event : events)
    //       list.add(event);

    //   Spinner spinner = new Spinner(context);

    //   ArrayAdapter<Event> adapter = new ArrayAdapter<>(context,android.R.layout.simple_list_item_1,list);

    //   spinner.setAdapter(adapter);

    //   addView(spinner);


   }

}
