package com.example.lynn.historychallenge;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * Created by lynn on 3/13/2015.
 */
public class MyDatabaseHelper extends SQLiteOpenHelper {
    private static int DATABASE_VERSION = 1;
    public Context context;

    public MyDatabaseHelper(Context context,
                            String name) {
        super(context,name,null,DATABASE_VERSION);

        this.context = context;
    }

    public void onCreate(SQLiteDatabase database) {
        MainActivity.textView.setText("I'm here");

        database.execSQL("DROP TABLE IF EXISTS events");

        database.execSQL("CREATE TABLE events(date TEXT NOT NULL,event TEXT NOT NULL);");

        database.execSQL("INSERT INTO events VALUES(\"1\",\"2\");");

        /*
        AssetManager manager = context.getAssets();

       String sql = "";

        try {
            InputStream stream = manager.open("events.sql");

            Scanner scanner = new Scanner(stream).useDelimiter("\\Z");

            sql = scanner.next().trim();

            scanner.close();
        } catch (IOException ie) {
            System.out.println(ie);
        }

        if (!sql.trim().equals(""))
            database.execSQL(sql);
            */
    }

    public void onOpen(SQLiteDatabase database) {
        MainActivity.textView.setText("open");
    }

    public void onUpgrade(SQLiteDatabase database,int oldVersion,int newVersion) {
        database.execSQL("DROP TABLE IF EXISTS events");

        onCreate(database);

    }

}
